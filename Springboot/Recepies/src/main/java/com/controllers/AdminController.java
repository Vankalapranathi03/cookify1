package com.controllers;

import com.dao.AdminDao;
import com.exceptions.UserNotFoundException;
import com.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/admin")
@CrossOrigin(origins = "http://localhost:4200")
public class AdminController {

    @Autowired
    private AdminDao adminDao;

    @GetMapping("/Allusers")
    public List<User> getAllUsers() {
        return adminDao.getAllUsers();
    }

    @GetMapping("/UserById/{userId}")
    public User getUserById(@PathVariable("userId") long userId) throws UserNotFoundException {
        return adminDao.getUserById(userId);
    }

    @PostMapping("/addusers")
    public User addUser(@RequestBody User user) {
        return adminDao.addUser(user);
    }

    @PutMapping("/users")
    public User updateUser(@RequestBody User user) {
        return adminDao.updateUser(user);
    }

    @DeleteMapping("deleteUserById/{userId}")
    public String deleteUserById(@PathVariable("userId") long userId) {
        adminDao.deleteUserById(userId);
        return "User with UserId: " + userId + " deleted successfully!";
    }

    @DeleteMapping("/deleteAllusers")
    public String deleteAllUsers() {
        adminDao.deleteAllUsers();
        return "All users deleted successfully!";
    }
}
