package com.controllers;

import com.dao.UserDao;
import com.exceptions.UserNotFoundException;
import com.model.OtpRequest;
import com.model.PasswordResetRequest;
import com.model.User;
import com.model.UserLogin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/auth")
@CrossOrigin(origins = "http://localhost:4200")
public class UserController {

    @Autowired
    private UserDao userDao;

    @PostMapping("/register")
    public ResponseEntity<User> registerUser(@RequestBody User user) {
        User savedUser = userDao.saveUser(user);
        return ResponseEntity.ok(savedUser);
    }

    @PostMapping("/login")
    public ResponseEntity<Map<String, String>> loginUser(@RequestBody UserLogin userLogin) {
        Map<String, String> response = new HashMap<>();
        System.out.println("Login attempt with email: " + userLogin.getEmailId());
        try {
            User user = userDao.loginUser(userLogin.getEmailId(), userLogin.getPassword());
            if (user != null) {
                response.put("status", "success");
                response.put("message", "Valid credentials");
                System.out.println("Login successful for email: " + userLogin.getEmailId());
                return ResponseEntity.ok(response);
            } else {
                response.put("status", "error");
                response.put("message", "Invalid credentials");
                System.out.println("Login failed for email: " + userLogin.getEmailId());
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
            }
        } catch (UserNotFoundException e) {
            response.put("status", "error");
            response.put("message", "Invalid credentials");
            System.out.println("Login failed for email: " + userLogin.getEmailId() + " - User not found");
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(response);
        }
    }


    @PostMapping("/forgot-password")
    public ResponseEntity<Map<String, String>> forgotPassword(@RequestBody OtpRequest otpRequest) {
        userDao.sendOtp(otpRequest.getEmailId());
        Map<String, String> response = new HashMap<>();
        response.put("message", "OTP sent to email!");
        return ResponseEntity.ok(response);
    }

    @PostMapping("/verify-otp")
    public ResponseEntity<Map<String, String>> verifyOtp(@RequestBody OtpRequest otpRequest) {
        boolean isValid = userDao.verifyOtp(otpRequest.getEmailId(), otpRequest.getOtp());
        Map<String, String> response = new HashMap<>();
        if (isValid) {
            response.put("message", "OTP verified!");
            return ResponseEntity.ok(response);
        } else {
            response.put("message", "Invalid OTP!");
            return ResponseEntity.badRequest().body(response);
        }
    }

    @PostMapping("/reset-password")
    public ResponseEntity<String> resetPassword(@RequestBody PasswordResetRequest resetRequest) {
        try {
            userDao.resetPassword(resetRequest);
            return ResponseEntity.ok("Password reset successfully!");
        } catch (UserNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }
}
