package com.controllers;

import com.dao.RecipeDao;
import com.model.Recipe;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/recipes")
@CrossOrigin(origins = "http://localhost:4200")
public class RecipeController {

    @Autowired
    private RecipeDao recipeDao;

    
    @GetMapping("/country/{countryName}")
    public List<Recipe> getRecipesByCountry(@PathVariable String countryName) {
        return recipeDao.getRecipesByCountry(countryName);
    }

    
    @GetMapping("/state/{stateName}")
    public List<Recipe> getRecipesByState(@PathVariable String stateName) {
        return recipeDao.getRecipesByState(stateName);
    }

   
    @GetMapping("/{id}")
    public Recipe getRecipeDetails(@PathVariable Long id) {
        return recipeDao.getRecipeDetails(id);
    }

  
    @PostMapping("")
    public Recipe addRecipe(@RequestBody Recipe recipe) {
        return recipeDao.addRecipe(recipe);
    }

  
    @PutMapping("/{id}")
    public Recipe updateRecipe(@PathVariable Long id, @RequestBody Recipe recipe) {
        return recipeDao.updateRecipe(id, recipe);
    }

   
    @DeleteMapping("/{id}")
    public void deleteRecipe(@PathVariable Long id) {
        recipeDao.deleteRecipe(id);
    }
}