package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String userName;
    private String country;
    private String emailId;
    private String password;
    private String phoneNumber;
    
    public User(){
    	
    }
    
	public User(Long id, String userName, String country, String emailId, String password, String phoneNumber) {
		super();
		this.id = id;
		this.userName = userName;
		this.country = country;
		this.emailId = emailId;
		this.password = password;
		this.phoneNumber = phoneNumber;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	@Override
	public String toString() {
        return "User [id=" + id + ", userName=" + userName + ", country=" + country + ", emailId=" + emailId
                + ", password=" + password + ", phoneNumber=" + phoneNumber + "]";
	}

	public User orElseThrow(Object object) {
	
		return null;
	}
}
