package com.model;

public class PasswordResetRequest {
    private String emailId;
    private String newPassword;

 
    public PasswordResetRequest() {}

    public PasswordResetRequest(String emailId, String newPassword) {
        this.emailId = emailId;
        this.newPassword = newPassword;
    }
    
    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
