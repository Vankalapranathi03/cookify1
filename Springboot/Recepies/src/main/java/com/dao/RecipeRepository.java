package com.dao;

import com.model.Recipe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RecipeRepository extends JpaRepository<Recipe, Long> {
    List<Recipe> findByCountry_Name(String countryName);
    List<Recipe> findByState_Name(String stateName);
    Optional<Recipe> findById(Long id);
    
}
