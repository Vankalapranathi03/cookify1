package com.dao;

import com.exceptions.UserNotFoundException;
import com.model.PasswordResetRequest;
import com.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@Service
public class UserDao {

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private JavaMailSender mailSender;

    private Map<String, String> otpStorage = new HashMap<>();

    private final BCryptPasswordEncoder bcryptEncoder = new BCryptPasswordEncoder();

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return bcryptEncoder;
    }

    public User saveUser(User user) {
        String encryptedPwd = bcryptEncoder.encode(user.getPassword());
        user.setPassword(encryptedPwd);
        return userRepo.save(user);
    }

    public User loginUser(String emailId, String password) throws UserNotFoundException {
        User user = userRepo.findByEmailId(emailId);
        if (user != null && bcryptEncoder.matches(password, user.getPassword())) {
            return user;
        } else {
            throw new UserNotFoundException("Invalid credentials");
        }
    }

    public void updatePassword(String emailId, String newPassword) throws UserNotFoundException {
        User user = userRepo.findByEmailId(emailId);
        if (user != null) {
            String encryptedPwd = bcryptEncoder.encode(newPassword);
            user.setPassword(encryptedPwd);
            userRepo.save(user);
        } else {
            throw new UserNotFoundException("User with email " + emailId + " not found!");
        }
    }

    public void sendOtp(String emailId) {
        String otp = generateOtp();
        otpStorage.put(emailId, otp);
        try {
            sendOtpEmail(emailId, otp);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    public boolean verifyOtp(String emailId, String otp) {
        return otp.equals(otpStorage.get(emailId));
    }

    public void resetPassword(PasswordResetRequest resetRequest) throws UserNotFoundException {
        String emailId = resetRequest.getEmailId();
        String newPassword = resetRequest.getNewPassword();
        User user = userRepo.findByEmailId(emailId);
        if (user != null) {
            String encryptedPwd = bcryptEncoder.encode(newPassword);
            user.setPassword(encryptedPwd);
            userRepo.save(user);
        } else {
            throw new UserNotFoundException("User with email " + emailId + " not found!");
        }
    }

    private String generateOtp() {
        Random random = new Random();
        int otp = 100000 + random.nextInt(900000);
        return String.valueOf(otp);
    }

    private void sendOtpEmail(String to, String otp) throws MessagingException {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        helper.setTo(to);
        helper.setSubject("Your OTP Code");

        String text = String.format(
                "Dear User,\n\n" +
                        "Your OTP code is: %s\n\n" +
                        "Please use this code to complete your verification. This OTP is valid for 10 minutes.\n\n" +
                        "If you did not request this code, please ignore this email or contact our support team.\n\n" +
                        "Thank you,\n" +
                        "Cookify",
                otp
        );
        helper.setText(text);
        mailSender.send(message);
    }
}
