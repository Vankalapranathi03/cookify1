package com.dao;

import com.model.Recipe;
import com.exceptions.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RecipeDao {

    @Autowired
    private RecipeRepository recipeRepository;

    public List<Recipe> getRecipesByCountry(String countryName) {
        return recipeRepository.findByCountry_Name(countryName);
    }

    public List<Recipe> getRecipesByState(String stateName) {
        return recipeRepository.findByState_Name(stateName);
    }

    public Recipe getRecipeDetails(Long id) {
        return recipeRepository.findById(id).orElseThrow(() -> new UserNotFoundException("Recipe not found with id " + id));
    }

    public Recipe addRecipe(Recipe recipe) {
        return recipeRepository.save(recipe);
    }

    public Recipe updateRecipe(Long id, Recipe recipeDetails) {
        Recipe existingRecipe = recipeRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("Recipe not found with id " + id));
        existingRecipe.setName(recipeDetails.getName());
        existingRecipe.setDescription(recipeDetails.getDescription());
        existingRecipe.setIngredients(recipeDetails.getIngredients());
        existingRecipe.setPreparation(recipeDetails.getPreparation());
        existingRecipe.setImage(recipeDetails.getImage());
        existingRecipe.setVideoUrl(recipeDetails.getVideoUrl());
        existingRecipe.setCountry(recipeDetails.getCountry());
        existingRecipe.setState(recipeDetails.getState());
        return recipeRepository.save(existingRecipe);
    }

    public void deleteRecipe(Long id) {
        Recipe existingRecipe = recipeRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("Recipe not found with id " + id));
        recipeRepository.delete(existingRecipe);
    }
}
