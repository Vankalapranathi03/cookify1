package com.dao;

import com.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findByEmailId(String emailId);

    @Query("SELECT u FROM User u WHERE u.emailId = :emailId AND u.password = :password")
    User loginUser(@Param("emailId") String emailId, @Param("password") String password);

}
