

import { Component, OnInit } from '@angular/core';
import { SavedRecipesService } from '../savedrecipe.service'; // Import SavedRecipesService

@Component({
  selector: 'app-savedrecipes',
  templateUrl: './savedrecipes.component.html',
  styleUrls: ['./savedrecipes.component.css']
})
export class SavedrecipesComponent implements OnInit {
  savedRecipes: any[] = [];

  constructor(private savedRecipesService: SavedRecipesService) {}

  ngOnInit(): void {
    this.savedRecipes = this.savedRecipesService.getSavedRecipes();
  }

  removeFromSavedRecipes(recipe: any) {
    this.savedRecipesService.removeFromSavedRecipes(recipe);
    this.savedRecipes = this.savedRecipesService.getSavedRecipes();
  }
}