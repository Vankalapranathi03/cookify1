import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent {
  emailId: string = '';
  newPassword: string = '';

  constructor(private userService: UserService, private router: Router, private toastr: ToastrService) {}

  onSubmit() {
    if (!this.emailId || !this.newPassword) {
      this.toastr.error('Email and password are required', 'Error');
      return;
    }

    const resetRequest = {
      emailId: this.emailId,
      newPassword: this.newPassword
    };

    this.userService.resetPassword(resetRequest).subscribe(
      () => {
        this.toastr.success('Password reset successful! Please login with your new password.', 'Success');
        this.router.navigate(['/login']);
      },
      (error: any) => {
        console.error('Error resetting password:', error);
        this.toastr.error('Failed to reset password. Please try again.', 'Error');
      }
    );
  }
}