// savedrecipes.service.ts

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SavedRecipesService {
  private savedRecipes: any[] = [];

  constructor() {}

  addToSavedRecipes(recipe: any) {
    if (!this.savedRecipes.some(r => r.id === recipe.id)) {
      this.savedRecipes.push(recipe);
    }
  }

  removeFromSavedRecipes(recipe: any) {
    const index = this.savedRecipes.findIndex(r => r.id === recipe.id);
    if (index !== -1) {
      this.savedRecipes.splice(index, 1);
    }
  }

  getSavedRecipes() {
    return this.savedRecipes;
  }
}