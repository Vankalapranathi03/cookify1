import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AuthService } from './auth.service';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { SavedrecipesComponent } from './savedrecipes/savedrecipes.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FoodrecipesComponent } from './foodrecipes/foodrecipes.component';
import { VerifyOtpComponent } from './verify-otp/verify-otp.component';
import { RequestOtpComponent } from './request-otp/request-otp.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component'; // Import ResetPasswordComponent
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FooterComponent } from './footer/footer.component';
import { CaptchaComponent } from './captcha/captcha.component';
import { CountryRecipesComponent } from './country-recipes/country-recipes.component';
import { StateRecipesComponent } from './state-recipes/state-recipes.component';
import { RecipeDetailsComponent } from './recipe-details/recipe-details.component';
import { ToastrModule } from 'ngx-toastr';
import { LogoutComponent } from './logout/logout.component';
import { BrowseComponent } from './browse/browse.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    AboutComponent,
    SavedrecipesComponent,
    NavbarComponent,
    FoodrecipesComponent,
    VerifyOtpComponent,
    RequestOtpComponent,
    ResetPasswordComponent, 
    FooterComponent,
    CaptchaComponent,
    CountryRecipesComponent,
    StateRecipesComponent,
    RecipeDetailsComponent,
    LogoutComponent,
    BrowseComponent,

  

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 3000, // toast message timeout in milliseconds (3 seconds here)
      positionClass: 'toast-top-right', // toast message position
      preventDuplicates: true, // prevent duplicate toast messages
    }),
    
    
   
  ],
  providers: [AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }