// recipe-details.component.ts

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RecipeService } from '../recipe.service';
import { SavedRecipesService } from '../savedrecipe.service'; // Import SavedRecipesService

@Component({
  selector: 'app-recipe-details',
  templateUrl: './recipe-details.component.html',
  styleUrls: ['./recipe-details.component.css']
})
export class RecipeDetailsComponent implements OnInit {
  recipe: any;
  currentRating: number = 0; // Initial rating
  stars: number[] = [1, 2, 3, 4, 5]; // Array to iterate over for stars
  isRecipeSaved: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private recipeService: RecipeService,
    private savedRecipesService: SavedRecipesService // Inject SavedRecipesService
  ) {}

  ngOnInit(): void {
    const recipeId = this.route.snapshot.paramMap.get('recipeId');
    this.recipeService.getRecipeDetails(recipeId!).subscribe(data => {
      this.recipe = data;
    });
  }

  formatPreparationSteps(preparation: string): string[] {
    if (!preparation) return [];
    return preparation.split('\n').filter(step => step.trim().length > 0);
  }

  saveRecipe() {
    if (!this.isRecipeSaved) {
      this.savedRecipesService.addToSavedRecipes(this.recipe);
    } else {
      this.savedRecipesService.removeFromSavedRecipes(this.recipe);
    }
    this.isRecipeSaved = !this.isRecipeSaved; // Toggle save state
  }

  rateRecipe(star: number) {
    this.currentRating = star;
    console.log('Rated:', star);
  }
}