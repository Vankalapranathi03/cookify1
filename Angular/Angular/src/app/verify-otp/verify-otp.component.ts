import { Component } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-verify-otp',
  templateUrl: './verify-otp.component.html',
  styleUrls: ['./verify-otp.component.css']
})
export class VerifyOtpComponent {
  emailId: string = '';
  otp: string = '';
  errorMessage: string = '';

  constructor(private userService: UserService, private router: Router, private toastr: ToastrService) {}

  verifyOtp() {
    this.userService.verifyOtp(this.emailId, this.otp)
      .subscribe({
        next: (response) => {
          console.log('OTP verification successful:', response);
          this.toastr.success('OTP verified!', 'Success');
          this.router.navigate(['/reset-password']);
        },
        error: (error: HttpErrorResponse) => {
          console.error('Error verifying OTP:', error);
          this.errorMessage = error.error.message || 'An error occurred during OTP verification';
          this.toastr.error(this.errorMessage, 'Error');
        }
      });
  }
}