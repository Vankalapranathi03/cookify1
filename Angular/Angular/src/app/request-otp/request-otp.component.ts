import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-request-password',
  templateUrl: './request-otp.component.html',
  styleUrls: ['./request-otp.component.css']
})
export class RequestOtpComponent {
  email: string = '';
  message: string = '';

  constructor(private userService: UserService, private router: Router, private toastr: ToastrService) {}

  onSubmit() {
    this.userService.requestPasswordReset(this.email).subscribe(
      (response: { message: string }) => {
        this.message = response.message;
        if (response.message === 'OTP sent to email!') {
          this.toastr.success('OTP sent successfully!', 'Success');
          this.router.navigate(['/verify-otp']);
        } else {
          this.toastr.error(response.message, 'Error');
        }
      },
      (error: any) => {
        this.toastr.error('Error sending OTP. Please try again.', 'Error');
      }
    );
  }
}