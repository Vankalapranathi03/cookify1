import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-captcha',
  templateUrl: './captcha.component.html',
  styleUrls: ['./captcha.component.css']
})
export class CaptchaComponent implements OnInit {
  captchaText: string | undefined;
  userInput: string = '';
  validationMessage: string = '';

  ngOnInit() {
    this.generateCaptcha();
  }

  generateCaptcha() {
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let result = '';
    const charactersLength = characters.length;
    for (let i = 0; i < 6; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    this.captchaText = result;
  }

  validateCaptcha(): boolean {
    if (this.userInput === this.captchaText) {
      this.validationMessage = 'CAPTCHA validated successfully!';
      return true;
    } else {
      this.validationMessage = 'Invalid CAPTCHA. Please refresh it.';
      return false;
    }
  }

  refreshCaptcha() {
    this.generateCaptcha();
    this.userInput = '';
    this.validationMessage = '';
  }
}
