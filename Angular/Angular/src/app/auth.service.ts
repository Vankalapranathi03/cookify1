declare var google:any;
import { Injectable,inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLoggedIn() {
    throw new Error('Method not implemented.');
  }
  router= inject(Router);
  requestPasswordReset(email: string) {
    throw new Error('Method not implemented.');
  }
  private baseUrl = 'http://localhost:8085/api/auth';

  constructor(private http: HttpClient) {}
  signOut(){
    google.accounts.id.disableAutoSelect();
    this.router.navigate(['']);
  }

  register(user: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/register`, user);
  }

  login(credentials: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/login`, credentials);
  }

  
}
