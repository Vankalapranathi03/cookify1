import { Component, HostListener, OnInit } from '@angular/core';
import { MediaObserver } from '@angular/flex-layout';
import { UserService } from '../user.service'; 

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  isMenuOpen: boolean = false;
  isMobile: boolean;
  isUserLoggedIn: boolean = false;

  constructor(private mediaObserver: MediaObserver, private userService: UserService) {
    this.isMobile = this.mediaObserver.isActive('xs') || this.mediaObserver.isActive('sm');
  }

  ngOnInit(): void {
    this.userService.getUserLoginStatus().subscribe(isLoggedIn => {
      this.isUserLoggedIn = isLoggedIn;
    });
  }

  toggleMenu(): void {
    this.isMenuOpen = !this.isMenuOpen;
  }

  closeMobileMenu(): void {
    if (this.isMobile) {
      this.isMenuOpen = false;
    }
  }

  logout(): void {
    this.userService.setIsUserLoggedOut();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: any): void {
    this.isMobile = event.target.innerWidth <= 1150;
    if (!this.isMobile) {
      this.isMenuOpen = false;
    }
  }
}
