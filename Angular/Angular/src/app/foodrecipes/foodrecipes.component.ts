import { Component, ElementRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-foodrecipes',
  templateUrl: './foodrecipes.component.html',
  styleUrls: ['./foodrecipes.component.css']
})
export class FoodrecipesComponent {
  @ViewChild('statesSection', { static: false }) statesSection!: ElementRef;

  states = [
    { name: 'Telangana', image: 'https://i.ytimg.com/vi/OnENyTjfens/maxresdefault.jpg' },
    { name: 'Andhra', image: 'https://www.whiskaffair.com/wp-content/uploads/2017/02/Ragi-Mudde-1-800x1198.jpg' },
    { name: 'Maharashtra', image: 'https://www.whiskaffair.com/wp-content/uploads/2018/12/Puran-Poli-2-1.jpg' },
    { name: 'Karnataka', image: 'https://www.whiskaffair.com/wp-content/uploads/2020/12/Paneer-Ghee-Roast-2-1.jpg' },
    { name: 'Tamil Nadu', image: 'https://www.whiskaffair.com/wp-content/uploads/2020/04/Rajma-Sundal-2-1.jpg' },
    { name: 'Uttar Pradesh', image: 'https://www.whiskaffair.com/wp-content/uploads/2018/01/Dahi-Vada-1.jpg' },
    { name: 'Gujarat', image: 'https://www.whiskaffair.com/wp-content/uploads/2019/06/Dabeli-1-1.jpg' },
    { name: 'Rajasthan', image: 'https://www.whiskaffair.com/wp-content/uploads/2021/04/Laal-Maas-2-1.jpg' },
    { name: 'West Bengal', image: 'https://www.whiskaffair.com/wp-content/uploads/2022/06/Tomato-Khejur-Chutney-2-1.jpg' },
    { name: 'Kerala', image: 'https://www.whiskaffair.com/wp-content/uploads/2018/05/Kerala-Fish-Curry-1.jpg' },
    { name: 'Kashmiri', image: 'https://www.whiskaffair.com/wp-content/uploads/2015/10/Kashmiri-Pulao-1-1.jpg' },
    { name: 'Punjabi', image: 'https://www.whiskaffair.com/wp-content/uploads/2019/05/Paneer-Butter-Masala-2-1.jpg' }
  ];

  countries = [
    { name: 'India', image: 'https://www.whiskaffair.com/wp-content/uploads/2015/10/Chicken-Shahjahani-2-1.jpg' },
    { name: 'America', image: 'https://www.whiskaffair.com/wp-content/uploads/2023/06/Pizza-Lava-Toast-2-1.jpg' },
    { name: 'Italian', image: 'https://www.whiskaffair.com/wp-content/uploads/2020/08/Chicken-Penne-Alla-Vodka-2-1.jpg' },
    { name: 'Chinese', image: 'https://www.whiskaffair.com/wp-content/uploads/2018/09/Chilli-Chicken-1.jpg' },
    { name: 'Mexican', image: 'https://www.whiskaffair.com/wp-content/uploads/2014/06/Churros-1.jpg' },
    { name: 'Thai', image: 'https://www.whiskaffair.com/wp-content/uploads/2021/10/Instant-Pot-Chicken-Congee-2-1.jpg' },
    { name: 'Caribbean', image: 'https://www.whiskaffair.com/wp-content/uploads/2020/07/Brown-Stew-Chicken-2-1.jpg' },
    { name: 'Middle Eastern', image: 'https://www.whiskaffair.com/wp-content/uploads/2021/03/Greek-Fries-2-1.jpg' },
  ];

  constructor(private router: Router) {}

  navigateToCountryRecipes(countryName: string) {
    if (countryName === 'India') {
      this.scrollToStates();
    } else {
      this.router.navigate(['/countries', countryName]);
    }
  }

  navigateToStateRecipes(stateName: string) {
    this.router.navigate(['/states', stateName]);
  }

  scrollToStates() {
    if (this.statesSection) {
      this.statesSection.nativeElement.scrollIntoView({ behavior: 'smooth' });
    }
  }
}
