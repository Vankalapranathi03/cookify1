import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RecipeService } from '../recipe.service';

@Component({
  selector: 'app-country-recipes',
  templateUrl: './country-recipes.component.html',
  styleUrls: ['./country-recipes.component.css']
})
export class CountryRecipesComponent implements OnInit {
  recipes: any[] = [];

  constructor(private route: ActivatedRoute, private recipeService: RecipeService) {}

  ngOnInit(): void {
    const countryName = this.route.snapshot.paramMap.get('countryName');
    if (countryName) {
      this.recipeService.getRecipesByCountry(countryName).subscribe(
        (data: any[]) => {
          this.recipes = data;
        },
        (error) => {
          console.error('Error fetching recipes:', error);
        }
      );
    }
  }
}
