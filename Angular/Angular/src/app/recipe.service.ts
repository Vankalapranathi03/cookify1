import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {
  private baseUrl = 'http://localhost:8085/api'; 

  constructor(private http: HttpClient) { }

  getRecipesByCountry(countryName: string): Observable<any[]> {
    return this.http.get<any[]>(`${this.baseUrl}/recipes/country/${countryName}`);
  }

  getRecipesByState(stateName: string): Observable<any[]> {
    return this.http.get<any[]>(`${this.baseUrl}/recipes/state/${stateName}`);
  }

  getRecipeDetails(recipeId: string): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/recipes/${recipeId}`);
  }
}
