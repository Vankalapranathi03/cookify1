import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { SavedrecipesComponent } from './savedrecipes/savedrecipes.component';
import { RegisterComponent } from './register/register.component';
import { FoodrecipesComponent } from './foodrecipes/foodrecipes.component';
import { RequestOtpComponent } from './request-otp/request-otp.component';
import { VerifyOtpComponent } from './verify-otp/verify-otp.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { CountryRecipesComponent } from './country-recipes/country-recipes.component';
import { RecipeDetailsComponent } from './recipe-details/recipe-details.component';
import { StateRecipesComponent } from './state-recipes/state-recipes.component';
import { BrowseComponent } from './browse/browse.component';
import { AuthGuard } from './auth.guard';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'foodrecipes', component: FoodrecipesComponent, canActivate: [AuthGuard] },
  { path: 'about', component: AboutComponent },
  { path: 'saved-recipes', component: SavedrecipesComponent, canActivate: [AuthGuard] },
  { path: 'request-otp', component: RequestOtpComponent },
  { path: 'verify-otp', component: VerifyOtpComponent },
  { path: 'reset-password', component: ResetPasswordComponent },
  { path: 'countries/:countryName', component: CountryRecipesComponent },
  { path: 'countries/:countryName/recipes/:recipeId', component: RecipeDetailsComponent },
  { path: 'states/:state', component: StateRecipesComponent },
  { path: 'states/:stateName/recipes/:recipeId', component: RecipeDetailsComponent },
  { path: 'browse', component: BrowseComponent },
  { path: 'recipe/:recipeId', component: RecipeDetailsComponent },
  { path: '', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
