import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private isUserLoggedIn: boolean = false;
  private loginStatus: Subject<boolean> = new Subject<boolean>();

  constructor(private http: HttpClient) {
  }

  setIsUserLoggedIn() {
    this.isUserLoggedIn = true;
    this.loginStatus.next(true);
  }

  setIsUserLoggedOut() {
    this.isUserLoggedIn = false;
    this.loginStatus.next(false);
  }

  getIsUserLoggedIn(): boolean {
    return this.isUserLoggedIn;
  }

  getUserLoginStatus(): Observable<boolean> {
    return this.loginStatus.asObservable();
  }

  isLoggedIn(): boolean {
    return this.getIsUserLoggedIn();
  }

  userLogin(user: any): Observable<any> {
    return this.http.post<any>('http://localhost:8085/api/auth/login', user, {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    });
  }

  registerUser(user: any): Observable<any> {
    return this.http.post('http://localhost:8085/api/auth/register', user);
  }

  requestPasswordReset(email: string): Observable<any> {
    return this.http.post('http://localhost:8085/api/auth/forgot-password', { emailId: email });
  }

  verifyOtp(email: string, otp: string): Observable<any> {
    return this.http.post('http://localhost:8085/api/auth/verify-otp', { emailId: email, otp: otp });
  }

  resetPassword(resetRequest: { emailId: string; newPassword: string }): Observable<any> {
    return this.http.post('http://localhost:8085/api/auth/reset-password', resetRequest, { responseType: 'text' });
  }

  getAllCountries(): Observable<any> {
    return this.http.get('https://restcountries.com/v3.1/all');
  }

  validateEmail(email: string): boolean {
    const pattern = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;
    return pattern.test(email.toLowerCase());
  }

}
