import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RecipeService } from '../recipe.service';

@Component({
  selector: 'app-state-recipes',
  templateUrl: './state-recipes.component.html',
  styleUrls: ['./state-recipes.component.css']
})
export class StateRecipesComponent implements OnInit {
  recipes: any[] = [];
stateName: any|string;

  constructor(private route: ActivatedRoute, private recipeService: RecipeService) {}

  ngOnInit(): void {
    const stateName = this.route.snapshot.paramMap.get('state');
    this.recipeService.getRecipesByState(stateName!).subscribe(data => {
      this.recipes = data;
    });
  }
}
